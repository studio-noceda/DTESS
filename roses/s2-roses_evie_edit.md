# Story 2: Roses
> Firstly, I want to ask: do you have a particular reason for picking first-person narration over third person?

> Second, it's hard to integrate naturally with a first person narrative, but there is actually no mention of who this character is within the story. Context makes it clear that it's Amity, but it's never explicitly stated.

I open the door to the shop. The shopkeeper greets, "Welcome in!". I've never done this before alone, I hope I do it right...

> I imagine the idea for this story was focused on Amity wanting to do something earth-romantic for Luz, or maybe you came from the angle of the monetary transaction first? Either way, it seems like you've brushed over some potentially interesting material here.

> Amity is buying a rose for the first time. Has she ever been into a florist? What is that experience like for the first time? How did she learn that a rose was an appropriate flower to buy? What is the shopkeeper like? Are they friendly? Intimidating? I imagine these things might influence how Amity would approach the situation.

"Hi", I awkwardly smile at her. "I'd like to buy Luz a rose."

> I like the way this dialogue is written. Amity specifying who the rose is for unnecessarily shows her awkwardness well, and also her interest.

Reaching into my pocket, past this _cell phone_ thing Camila said I needed, I grab a dollar. I'm not sure why it's called a dollar. I don't know many of these Earth things. It does have the number 10 written on it, which I hope means I have enough.

"Just one?" she asks. Oh no, am I supposed to get more?

"Is that okay?" I ask. Getting a little flustered, I re-tuck the hair behind my ear.

> I think you could add more description here about Amity's reaction.

"If you want just one, that's fine." Phew!

Now, let's see how this human technology works. Does the number on it go down? Do they print a new one? Camila always uses a plastic card, so I've never seen this before.

"That'll be 6 dollars."

I hand her the dollar, and she takes it. Opening the register, she puts it into a tray, and takes four identical looking dollars out of another tray.

"Here's your change!", she drops them into my hand and smiles at me. "I'll be right back with your rose." That was more disappointing than I thought. They count money by just exchanging papers with numbers on them?

> If this is Amity's first time using money, you could probably put some more description on how these notes differ to her from the other one. You only mentioned the number on the 10, so while the reader can intuit these are singles, Amity hasn't "noticed" that in the narration.

She grabs one from behind the counter, and wraps it nicely. "Here you go."

> This is another thing I think you could add more detail to. How is it wrapped? Would Amity ask for anything in particular? Does she mention that it's for her girlfriend?

I head towards the door, but I forgot something. 

> Another place to add more detail to Amity's actions. When did she realise she forgot? Did she have to turn around? Come back inside? 

"Thank you!" I exclaim. The clerk looks a little surprised, but smiles and waves. Why did I say that so loudly? Luz told me to use manners, they really matter. These humans are so polite!

Waving to her with a smile, I then turn and walk out of the store. _Eeeeeeeeeee!_ One pretty rose for my perfect girlfriend! Wait, did I just do a little dance in front of this store?

> Another opportunity to add some more detail. Did something trigger Amity to be more self aware in this moment? Would she try to play it off, if people are around to see it? 

> Overall I really like this conceptually. You've picked a strong idea that is ripe for like, very traditional fanfiction. I think you've gone through with a very strong idea of where you wanted to end up at, but it could benefit from spending a little more time stopping to smell the roses. 

> As a vignette there doesn't need to be an arc, but you've provided a lot of emotional beats here. Amity has a motivation, and it's realised by the end of the short. She's got small tensions and conflicts to overcome. I think you have all the fundamental pieces in here, they can just shine a bit more with polish. 