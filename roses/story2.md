# Story 2: Roses

I open the door to the shop. The shopkeeper greets, "Welcome in!". I've never done this before alone, I hope I do it right...

"Hi", I awkwardly smile at her. "I'd like to buy Luz a rose."

Reaching into my pocket, past this _cell phone_ thing Camila said I needed, I grab a dollar. I'm not sure why it's called a dollar. I don't know many of these Earth things. It does have the number 10 written on it, which I hope means I have enough.

"Just one?" she asks. Oh no, am I supposed to get more?

"Is that okay?" I ask. Getting a little flustered, I re-tuck the hair behind my ear.

"If you want just one, that's fine." Phew!

Now, let's see how this human technology works. Does the number on it go down? Do they print a new one? Camila always uses a plastic card, so I've never seen this before.

"That'll be 6 dollars."

I hand her the dollar, and she takes it. Opening the register, she puts it into a tray, and takes four identical looking dollars out of another tray.

"Here's your change!", she drops them into my hand and smiles at me. "I'll be right back with your rose." That was more disappointing than I thought. They count money by just exchanging papers with numbers on them?

She grabs one from behind the counter, and wraps it nicely. "Here you go."

I head towards the door, but I forgot something. "Thank you!" I exclaim. The clerk looks a little surprised, but smiles and waves. Why did I say that so loudly? Luz told me to use manners, they really matter. These humans are so polite!

Waving to her with a smile, I then turn and walk out of the store. _Eeeeeeeeeee!_ One pretty rose for my perfect girlfriend! Wait, did I just do a little dance in front of this store?