# Story Plan

This story in Down to Earth: Short Stories is a "vignette" about a cute moment in the life of Masha and Vee. This establishes the tone and story-telling of _DtE:SS_ as a collection of short snapshots of Gravesfield life, similar in concept to _The Simpsons_'s episode _22 Short Films About Springfield_. This collection of stories is meant to mostly be one-shot writing pieces that can be loosely connected together. I believe writing short stories like this will help hone the craft of plot planning and tight writing "timing".

## Short Summary

Camila is wondering about a new "101 Bug Facts" book on the kitchen counter, when she overhears a cute romance moment, Amity is giving Luz a flower. It's absolutely adorable!

## Plot Outline

- Camila picks up a new book on the coffee table, and inspects it
- 101 Insects? I don't remember buying that.
- Notice the library asset tag, it must have been Amity.
- Hear Amity call out "Luz! I have something for you!", it must be something important or romantic, I better go look
- Peep through the opening to the kitchen, Amity is holding a rose
- Luz comes down, and loves her gift
- Camila comments on the cute couple, and Amity feels slightly embarrassed
- Let's go put the rose in a vase on the coffee table

## Plot Continuity & Canon Notes

Luz and Amity and their rose will be seen by Willow in the next story