# Overall Planning

## Schedule

Stories in order:

1. Vee and Amity, Vee sees Valentine's Day on the calendar when she flips the page for the start of the month. Tells Amity about it, and Amity goes off to do something for Luz without checking that it is actually Valentine's Day.
   
2. Masha meets Vee at the park, Vee is looking through a lightly forested park at Amity before being interrupted to be shown a bug fact and get a book.
   
3. The florist cashier deals with a flustered Amity coming into the store. Amity is buying a rose for Luz because she's heard from a magazine that humans love roses, and they are romantic.
   
4. The café cashier overhears Willow read aloud a text from Vee asking if she saw Amity near the florist. Willow has been speaking to her phone to search for information about the Boiling Isles, but not getting anything useful, just some human world stuff that is confusingly similar. (Grimwalker, Grimalkin?). The cashier wonders about these weird new foreign kids.
   
5. Camila is wondering about a new "101 Bug Facts" book on the kitchen counter, when she overhears a cute romance moment, Amity is giving Luz a flower. It's absolutely adorable!
   
6. Willow comes home and meets Amity and asks about the florist shop, since Vee asked her about it. Amity says she bought a rose because it's Valentine's Day, and Willow says that's very cute. What are you guys going do on Valentine's Day, though? (It's not for another 13 days)