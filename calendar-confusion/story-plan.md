# Story Plan

This story in Down to Earth: Short Stories is a "vignette" about a cute moment in the life of Masha and Vee. This establishes the tone and story-telling of _DtE:SS_ as a collection of short snapshots of Gravesfield life, similar in concept to _The Simpsons_'s episode _22 Short Films About Springfield_. This collection of stories is meant to mostly be one-shot writing pieces that can be loosely connected together. I believe writing short stories like this will help hone the craft of plot planning and tight writing "timing".

## Short Summary

Vee and Amity, Vee sees Valentine's Day on the calendar when she flips the page for the start of the month. Tells Amity about it, and Amity goes off to do something for Luz without checking that it is actually Valentine's Day.

## Plot Outline

- Amity and Gus are eating breakfast in the kitchen of the family home, while Vee flips over the calendar, since it just became February.
- Vee notices Valentine's Day marked as a holiday on the calendar. She knows Amity and Luz are dating, so she better tell Amity about it, it's a human tradition.
- Vee casually describes it to Amity
- Amity panics and runs out the door and out the house with her food, to go buy a gift for Luz. Amity didn't bother to ask if it was today (it isn't).
- Vee is left confused

## Plot Continuity & Canon Notes

Vee would later see Amity around the florist shop Amity will go to later in the story *Veetle*, and ask Willow via text message about this.

Amity will later come home to gift Luz the rose, and Camila will overhear a cute couple event happening and watch.