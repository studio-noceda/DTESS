# Calendar Confusion

>> Feels like off "character voice"
Hmm, I guess it's now February, isn't it? Seeing the months fly by since I got here has been quite something. I've had more time than they have, they must be feeling even more dizzy. 

I should probably flip the calendar over and have a look at this month's events... Gus's birthday, Valentine's Day, President's Day... oh! I should tell Amity about Valentine's Day!

"Hey Amity! I have a human tradition for you!", I turn around and look at her, hand on the calendar page still. She looks up at me, cereal bowl now left neglected, a smile lights up her face. Gus looks at me too, from his plate of toast.

"What is it?" she asks.

"It's called _Valentine's Day_", I smile at her. "It's where you do something nice for your love, to make them feel appreciated."

"Do I not do that enough? Have I been neglecting my Luz?", her face going red, frowning at the thought.

"No... you two seem pretty happy together. Why would you think that? It's a day for doing something a little more special, give her flowers or take her to dinner. It's supposed to be fun, not an obligation."

She takes a deep breath, and composes herself. "I'll make it Luz's special day! It's my promise to her!" she says with confidence in her step, as she triumphantly gets up from her chair, and runs out of the room.

"Wait, Amity!" I call after her, but she's gone out the front door now. That girl has a lot of determination, that's for sure. I look at Gus, and he shrugs at me, and goes back to eating his toast.

I am pretty hungry now that I think about it, I should probably have something to eat before I meet up with Masha. Amity can't get into too much trouble, I don't think.