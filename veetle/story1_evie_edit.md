# Story 1: Veetle

I look back and forth for my friend, scanning through the trees and parkways. I told her to meet me at the park, I hope she got my text... _there she is_. 

> You should provide more description here -- the reader doesn't know what Vee is doing or what that looks like to Masha, so there's no context for Vee's surprise, or Masha's interest or confusion in what she is doing.

Taking the path, I walk over to her.

"Hey, Vee."

Vee gasps, and turns around in a bit of a panic. "Masha! I didn't notice you..."

"Whatchya lookin' at?" I ask. She did seem pretty interested in something.

"Oh, uh, nothing. Just looking at this tree."

"There are a lot of trees to study here. Maybe you'll get an A in tree identification, you seem pretty intent on it." I joke, smiling at her.

> I think moving the bag detail further down helps keep the conversation flowing here. By introducing it a couple of paragraphs before you resolve the bag opening, it creates tension for the reader which doesn't need to be built.

"Is that an exam? Do I need to know all the trees? Nobody told me I needed to know that for school!". She looks pretty panicked. The things this girl doesn't know always surprises me.

"No... you thought that was a school subject? To know all the trees? There are tens of thousands of tree species. It'd be pretty hard!"

"It would be a little silly... I guess". Her cheeks are a bit rosy, and she shyly looks down at her foot.

"Hey, it's okay. Not everyone knows everything. Speaking of which, I brought you something". I lower my hand to the buckle of my satchel and open it, pulling out a book, and flip it open to the bookmarked page. 

"You went to the zoo recently, and you kept asking me about all the animals, so I checked out this book for you at the library! It's about bugs and insects. You might like it."

> The way you have worded the following paragraph is a little stilted. You do want your narration to describe what the character is doing, but when you don't attach a pronoun/noun to the verb (in this case, no noun or pronoun is attached to the verb "looking") it sounds more awkward. This is a weird english rule that nobody ever really explains, so I understand how it isn't apparent.

Looking down at the page, I scan the words looking for the fun fact I wanted to tell her... here it is! "Did you know some rhinoceros beetles can lift up to 30 times their own weight?"

Vee looks pretty concerned. "Those beetles are pretty scary. I ran away from one once, I thought it was going to get me."

"You ran away... from a *beetle?* It was going to get you?" That's definitely a new one. I give her a puzzled expression.

"I didn't want it to pick me up!" she says, exasperated.

"Vee, have you actually seen a beetle? Do you know what they look like?"

She once again looks shy, and scrunches herself together a little. "No..."

> Another weird rule of narration. You need to separate narration (what the character is doing) and internal voice (what the character is thinking). 

I giggle. I just can't believe this girl! She must have spent all her time indoors. 

Turning the book to face her, I point to the photo of the beetle on the page. A fearsome foe, sitting atop a leaf. "See, look! Beetles are pretty cool. They're only small, so they're not going to get you. Well, maybe they would, if they could."

> I just made it a little clearer how Masha was countering the idea of the beetles not being able to get Vee. I dont think you need to do it in the way i changed it, but i think it's important to make it a bit more direct.

Vee slinks back a little.

"Kidding, of course. You're way too big for them to get you."

I hand the book over to her. "Have a read through the book. You can hold onto it for a little, but I need it back sometime. You do know how libraries work, do you?"

"Yeah, there was one back home."

"Do you _actually_ know how libraries work?"

"Yes! I have actually been to one, Masha."

I give her a suspicious look, and then smile. "So you _have_ left your house." I have to show her the world, she doesn't seem to show it to herself. I just hope I don't scare her away. Then again, I'd probably have scared her off already, if I would.

> You are very strong with writing dialogue. Both character's voices feel distinct and authentic to their character in the show, so that's really well achieved.

> I agree with Crystal, I think you could spend more time describing the scene and the actions and movements of characters. It's not something I would say is done *badly* though, I was never confused by a character suddenly being somewhere or having done something I didn't notice. But having a little bit more is helpful for keeping the scene grounded, and the image strong in reader's minds.

> I think this is especially true for facial expressions. You have a few instances of saying that Vee looks "pretty something". While it's not something you always need to avoid, I think you can improve your point of view by describing some of what Masha sees, which indicates to her that Vee is feeling a certain way. You've done this very well in other places, where Vee shrinks or scrunches in when she feels embarrassed, her cheeks are rosy and she looks down at her feet. This is what people mean when they say show, don't tell. When you indirectly communicate things to readers, it keeps them more actively invested in your writing.

> Overall, I think this is very good. You're clearly very good at getting into the character's mind and voice, which is something a lot of people can really struggle with. It's a hugely important skill for writing compelling characters. I think you can refine this easily to make it more engaging, but what you have already is very solid for a first draft!