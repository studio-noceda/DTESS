# Story 1: Veetle

I look back and forth for my friend, scanning through the trees and parkways. I told her to meet me at the park, I hope she got my text... _there she is_. Taking the path, I walk over to her.

"Hey, Vee."

Vee gasps, and turns around in a bit of a panic. "Masha! I didn't notice you..."

"Whatcya lookin' at?" I ask. She did seem pretty interested in something.

"Oh, uh, nothing. Just looking at this tree."

"There are a lot of trees to study here. Maybe you'll get an A in tree identification, you seem pretty intent on it." I laugh, and lower my hand to the buckle of my satchel.

"Is that an exam? Do I need to know all the trees? Nobody told me I needed to know that for school!". She looks pretty panicked. The things this girl doesn't know always surprises me.

"No... you thought that was a school subject? To know all the trees? There are tens of thousands of tree species. It'd be pretty hard!"

"It would be a little silly... I guess". Her cheeks flush red, and she shyly looks down at her foot.

"Hey, it's okay. Not everyone knows everything. Speaking of which, I brought you something". Unclasping the bag buckle, I pull out a book, and flip it open to the bookmarked page. "You went to the zoo recently, and you kept asking me about all the animals. I checked out this book for you at the library, it's about bugs and insects. You might like it."

Looking down at the page, looking for the fun fact I wanted to tell her... here it is. "Did you know some rhinoceros beetles can lift up to 30 times their own weight?"

Vee looks pretty concerned. "Those beetles are pretty scary. I ran away from one once, I thought it was going to get me."

"You ran away... from a *beetle?* It was going to get you?" That's definitely a new one. I give her a puzzled expression.

"I didn't want it to pick me up!" she says, exasperated.

"Vee, have you actually seen a beetle? Do you know what they look like?"

She once again looks shy, and scrunches herself together a little. "No..."

Giggling, I just can't believe this girl! She must have spent all her time indoors. Turning the book to face her, I point to the photo of the beetle on the page. A fearsome foe, sitting atop a leaf. "See, look, beetles are pretty cool. They're not going to get you. Well, maybe they would, if they could."

Vee slinks back a little.

"Kidding, of course. You're way too big for them to get you."

I hand the book over to her. "Have a read through the book. You can hold onto it for a little, but I need it back sometime. You do know how libraries work, do you?"

"Yeah, there was one back home."

"Do you _actually_ know how libraries work?"

"Yes! I have actually been to one, Masha."

I give her a suspicious look, and then smile. "So you _have_ left your house." I have to show her the world, she doesn't seem to show it to herself. I just hope I don't scare her away. Then again, I'd probably have scared her off already, if I would.