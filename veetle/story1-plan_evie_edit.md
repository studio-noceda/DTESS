# Story 1 Plan

Story 1 of Down to Earth: Short Stories is a "vignette" about a cute moment in the life of Masha and Vee. This establishes the tone and story-telling of _DtE:SS_ as a collection of short snapshots of Gravesfield life, similar in concept to _The Simpsons_'s episode _22 Short Films About Springfield_. This collection of stories is meant to mostly be one-shot writing pieces that can be loosely connected together. I believe writing short stories like this will help hone the craft of plot planning and tight writing "timing".

## Short Summary

Masha checks out a book at the town library, and shows it to Vee. Vee is amused by the fantastical sounding things in it, which are totally normal to people of Earth.

## Questions to answer

**What is the book about?**

Masha is quite interested in the mysterious and mythical, but these things seem to make Vee uncomfortable (since they usually involve the Boiling Isles, the place she's trying not to let anyone know she's from!). Still, I think it seems likely that Masha would pick a book of interesting and probably obscure facts to show to Vee. Perhaps something about animals? They're quite tied to mythology. 

>Perhaps Vee brings up the trip to the zoo? And Masha checks out a book on animals because of that.

I like to imagine Vee is interested to know about all the creatures around, given that the Boiling Isles is so diverse in sentient life and much more connected to BI residents' daily lives, it seems only natural. It's probably a little weird to her that they don't constantly try to eat people, though! Vee is never sure if what is normal in the Boiling Isles is normal in real life...

>While this is almost certainly true, remember that Vee's familiarity with the human realm is probably similar to Luz's familarity with the Boiling Isles at this point. Not encyclopedic, but a working knowledge. Maybe use Luz as a point of reference?

I think it's probably best if the fact(s) is about some small bug that isn't *incredibly* common, it would seem unbelievable to Vee that things would be that small.

**What does Vee get confused about, thinking is and isn't real?**

The idea of small and non-people-violent animals is pretty novel to Vee. She also doesn't know what Earth people name all their stuff. Almost everything in the Boiling Isles wants to eat you! Things seem unusually calm on Earth.

**How does Masha react to Vee's confusion?**

Vee's reactions to things and lack of knowledge seem rather curious, but in an innocent & sheltered way. Masha is amused with teaching Vee stuff, and feels like Vee should know more about the world around her. It's kind of cute when Vee seems a little perplexed by things.

>Do you think Masha would also be a little bit concerned, maybe? Like, worried that Vee is too sheltered, or hasn't been able to experience much in her life... it's hard to say

**What cute, informational, or interesting character moments can happen?**

Masha trying to teach her friend & possible crush about the world, in a bit of a teasing manner. Vee trying to keep her guard up about fitting in and being human, but allows herself to admit she doesn't know something to her closest ally of this crazy world.

## Plot outline

- Masha finds Vee at the park, Vee is curiously looking at something
- Masha greets Vee, and catchers her by surprise
- Vee tries to play it cool
- Masha asks about what Vee was so interested in
- Vee acts coy about the subject
- Masha mentions the book she just got, we should check it out
- She opens the book to a page, it's a page about a small animal
- Masha recites a fun fact from the book, expecting Vee to know about the common animal she's talking about
- Vee acts like she knows what the animal is, by making up something that ends up being outrageous
- Masha acts confused, haven't you ever seen these?
- Vee admits to not having seen it
- Masha acts excited to teach Vee a new thing, and hands her the book at the page
- Masha thinks to herself "this girl is so weird, how could she not know this?", but in like, the cute and alluring "I love them" way

## Plot continuity & canon notes

A cute story for Vee and Masha to remember in the future, perhaps. Vee is slightly less confused about Earth topics, and may have a more active interest in learning/where she can do it.