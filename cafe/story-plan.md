# Story Plan

This story in Down to Earth: Short Stories is a "vignette" about a cute moment in the life of Masha and Vee. This establishes the tone and story-telling of _DtE:SS_ as a collection of short snapshots of Gravesfield life, similar in concept to _The Simpsons_'s episode _22 Short Films About Springfield_. This collection of stories is meant to mostly be one-shot writing pieces that can be loosely connected together. I believe writing short stories like this will help hone the craft of plot planning and tight writing "timing".

## Short Summary

The café cashier overhears Willow read aloud a text from Vee asking if she saw Amity near the florist. Willow has been speaking to her phone to search for information about the Boiling Isles, but not getting anything useful, just some human world stuff that is confusingly similar. (Grimwalker, Grimalkin?). The cashier wonders about these weird new foreign kids.

## Plot Outline

- The cashier has been listening to Willow and Gus searching for information about weird witching things
- Willow gets a text from Vee asking if she has seen Amity around the florists
- Willow is confused by this, and asks Gus about what he thinks of the request
- They decide to head out of the store to go home and check on Vee
- The cashier is left to wonder about these strange new kids

## Plot Continuity & Canon Notes

Willow will later come home to investigate the weird request from Vee, but will notice Amity and Luz being together and the rose, and ask what they're gonna do for Valentine's