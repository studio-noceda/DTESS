# Masha

- [Masha](#masha)
  - [Quick Overview](#quick-overview)
  - [Character Questionnaire](#character-questionnaire)
    - [Basics](#basics)
      - [Names:](#names)
      - [Birth Info](#birth-info)
    - [Physical Information](#physical-information)
      - [Body](#body)
      - [Head](#head)
      - [Skin](#skin)
      - [Hands](#hands)
      - [Make-up](#make-up)
      - [Scars, Birthmarks, and Tattoos](#scars-birthmarks-and-tattoos)
      - [Physical Handicaps and Disabilities](#physical-handicaps-and-disabilities)
      - [Type(s) of Clothing](#types-of-clothing)
      - [Mannerisms](#mannerisms)
  - [Character Notes](#character-notes)
    - [Personality](#personality)
    - [Gender](#gender)
  - [Relationships](#relationships)
    - [Vee Noceda](#vee-noceda)
    - [Luz Noceda](#luz-noceda)
    - [Jacob Hopkins](#jacob-hopkins)
    - [Amity, Willow, and Gus](#amity-willow-and-gus)
    - [Camp Friends (Krysa and June)](#camp-friends-krysa-and-june)
    - [Parents](#parents)

## Quick Overview

Masha is a young 15-year-old enby (they/them) who is into the supernatural and weird facts. They enjoy their circle of close friends, spending a lot of time with their friends. They're very loyal, and outgoing to the people willing to be interested in their weird topics.

Masha is a social chameleon, they are very adept at fitting whatever the situation calls for. They like learning about and respectfully participating in various cultures and events. They can slow down to meet the pace of a shy person who needs some help, or be weird and outgoing enough for a group of loud boisterous nerds.

Masha probably has high functioning autism.

## Character Questionnaire

### Basics

#### Names:

| First Name | Middle Names(s) | Last Name | Nicknames |
|------------|-----------------|-----------|-----------|
| Masha      |                 |           |           |

#### Birth Info

| Date of Birth | Age |
|---------------|-----|
|               | 15  |

### Physical Information

**Other distinguishing features?**

#### Body

| Height         | Weight | Build | Race          |
|----------------|--------|-------|---------------|
| Medium-average | Slim   | Slim  | White-passing |

**Which bodily feature is most prominent?** Long, non-binary flag colour painted fingernails

#### Head

| Hair colour | Hair style                                                  | Eye colour | Eye shape | Glasses? | Distinguishing facial features                                            |
|-------------|-------------------------------------------------------------|------------|-----------|----------|---------------------------------------------------------------------------|
| Black       | Short hair with tufts down the side, and spikes at the back | Dark brown | Almond    | No       | Missing front tooth right in the middle, and their upper lip bows upwards |

**Which facial feature is most prominent?** Their upper lip tends to be furled up, with their front teeth poking out, missing tooth on display.

#### Skin

**Anything of interest with skin features?**

#### Hands

**Anything of interest with the characters hands?** Long, non-binary flag colour painted fingernails.

#### Make-up

Nothing

#### Scars, Birthmarks, and Tattoos

Nothing

#### Physical Handicaps and Disabilities

Nothing

#### Type(s) of Clothing

They like dark, feminine clothing. They're often wearing a black dress with grey tights, and wear black boots with a brown back heel part.

Their Gravesfield Historical Society uniform is a green T-shirt with "G.H.S" written on it, with "Staff" in a fancy furled banner under the letters.

#### Mannerisms

They tend to be pretty emphatic and gestural. They are very respectful of people's boundaries. Masha likes to get comfortable anywhere they sit.

## Character Notes

### Personality

- **Loyal** - Masha loves to be close to their friendship circles, and be the glue that keeps the groups together. They're up to the challenges of friendship, and keeping their secrets.
- **Outgoing** - if you can keep up with their favourite subjects, or teach them some new fun and weird facts, they really open up! They love to help people, and teach them new stuff. It does help they tend to hang out in the places that other people interested in the weird and wacky show up to.
- **Helpful** - if there's ever a friend in need, they're up to help. Bring in the groceries, build some furniture, hide some bodies. Okay, maybe not that last one, hopefully.
- **Loves the supernatural** - they absolutely love the weird and spooky, which is why they work for the Gravesfield Historical Society. The GHS has plenty of witch-y, weird information and facts. They hope some of it is true!
- **Outdoorsy** - Masha likes the outdoors, if there's two places they like to be, it's in a library or out enjoying the fresh air. Parks are a nice place to hang out, and forests are a good place to see the fun local wildlife they know so much about.

### Gender

Masha is non-binary, they go by they/them. **Do not speculate on Masha's sex**, part of the character is being mysterious about their sex/gender but also completely comfortable with who they are. They're just Masha.

## Relationships

### Vee Noceda

Future crush, Vee is the weird, suspicious girl Masha finds so fascinating. Vee seems familiar, they swear Luz's voice sounds the same. Sounded? And that interesting hair. It's like some of her hair is like pet ears. She also never seems to have been leaving the house before coming to Gravesfield, she seems weirdly lost on human stuff sometimes. Vee has to pretend to not know Masha, or relate to the stories of things Masha has told her before. Or the events "Luz" was there for.

### Luz Noceda

Masha knew this person for a while, but Luz suddenly became pretty different, and became distant. They hope they didn't drive Luz away. They've tried to ask Luz about it, but she always seems to be busy and want to leave. As much as Masha feels a sense of duty to make sure Luz is okay, Luz clearly wants to be left alone.

### Jacob Hopkins

The previous museum curator, Masha really doesn't like this guy. His theories and notes are interesting, but spending time with him in the same room, he is insufferable. There is some weird notes about seeing a shape-shifting creature in person, but that couldn't possibly actually be real...

### Amity, Willow, and Gus

Regulars of the Historical Society, Masha likes to help them on their research, although they seem a little closeted on what exactly they're looking for. They seem like cool people, though. Would definitely like to hang out with this group sometime.

### Camp Friends (Krysa and June)

Krysa (she/they) is the white and blue shirt girl, and June (he/him) is the purple shirt guy.

They spent summer camp together in a cabin with Vee, all of them are pretty good friends with each other. They're not as into the ghostly as Masha is, but still find it quite entertaining to listen to Masha talk about it. Everyone would all have a lot of fun camp stories together.

### Parents

Masha's parents are pretty accepting and available for their kid. They're very proud of the happy, upstanding person their child has become. Masha likes their parents, but of course being in their teens, they like their independence and forging their own way. A pretty healthy parental relationship with both their parents.