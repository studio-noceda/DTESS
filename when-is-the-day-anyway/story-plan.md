# Story Plan

This story in Down to Earth: Short Stories is a "vignette" about a cute moment in the life of Masha and Vee. This establishes the tone and story-telling of _DtE:SS_ as a collection of short snapshots of Gravesfield life, similar in concept to _The Simpsons_'s episode _22 Short Films About Springfield_. This collection of stories is meant to mostly be one-shot writing pieces that can be loosely connected together. I believe writing short stories like this will help hone the craft of plot planning and tight writing "timing".

## Short Summary

Willow comes home and meets Amity and asks about the florist shop, since Vee asked her about it. Amity says she bought a rose because it's Valentine's Day, and Willow says that's very cute. What are you guys going do on Valentine's Day, though? (It's not for another 13 days)

## Plot Outline

- Willow and Gus get to the front door, discussing the day
- Open the door and look over to see Amity and Luz on the sofa, with the rose on the coffee table
- Aww, that's adorable! Did Amity get that?
- What're you going to do for Valentine's Day?
- Amity realises she didn't actually ask what day it is on

## Plot Continuity & Canon Notes

N/A