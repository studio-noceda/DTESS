# Tiny Stories
Maybe good for comics, or other micro/short stories.

- Vee tries to use a washer/drier, and it beeps angrily at her. She hisses at it, and Masha comes to the rescue

---

Masha's Stories

Fanfic: vee admits to masha that she was luz, and then do life stuff together, and get together 

Vee looks into a mirror and is studying her new face, and then her new hair, worried about how to tell masha she has actually been luz. How well she tell her? She shouldn’t know I’m a basilisk. But she’ll have to, really. They’ll wonder why luz is suddenly distant and confused by them, if not. And “Luz” is pretty similar to me. We could go over how Vee came to Earth and some stuff that happened at camp to see if there’s anything we can come up with to explain out of this (there isn’t, but it’s good lore!)

Masha, however, has read about basilisks, and recognised the ears when they met Vee

Chapter 1: The Tour - before the gang leaves the historical society, Vee takes up Masha’s offer for a tour. The next day or two after, Masha meets up with Vee to do the tour, during which Masha “introduces” to Vee that the town has a strong history with witches, while taking her around and getting tea. Something seems off about this Vee, besides her voice sounding like Luz. 

Chapter 2: Jacob’s Monster - Masha goes through Jacob’s documents he left behind, and the old security footage, and notices those familiar ears. There definitely is something strange about this Vee girl now. Masha decides to spend more time with Vee to see if they can get her to open up, or notice anything more. Masha tells Vee about Jacob’s recordings. Vee doesn’t want to open up much about her past though, making some stuff up, while crushing on Masha. 

Chapter 3: Vee’s Confessions - On another trip to the cafe, Vee has two things to admit to Masha. She has a crush on them, and she’s not quite what she seems. Vee spills the details about being the shapeshifting creature Masha knows about, but is still reluctant to share much about her past self. Her friends are dimensional travellers like her, and are on an important mission without much detail. Masha offers to help Vee unravel the secrets of this town, which she agreed to do.

A joke could be that Vee initially texts with Masha very formally, and Masha replies back “u don’t have 2 text so formal u dork ;p”